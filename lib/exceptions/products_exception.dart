class ProductsException implements Exception {
  final String message;

  ProductsException({this.message = 'Unknown error occurred. '});
}
