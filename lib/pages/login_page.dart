import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/locator.dart';
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/login_styles.dart';
import 'package:gedauto/utils/network_utils.dart';
import 'package:gedauto/widgets/error_dialog_box.dart';
import 'package:gedauto/widgets/login/form.dart';
import 'package:gedauto/widgets/login/login_animation.dart';
import 'package:gedauto/widgets/login/login_button.dart';
import 'package:gedauto/widgets/login/tick_login.dart';

import '../blocs/blocs.dart';
import '../services/services.dart';

class LoginAnimatedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          Container(child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          // ignore: close_sinks
          final authBloc = BlocProvider.of<AuthenticationBloc>(context);
          if (state is AuthenticationNotAuthenticated) {
            return _AuthForm();
          }
          if (state is AuthenticationFailure) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(state.message),
                TextButton(
                  child: Text(txt('retry'),
                      style: TextStyle(color: Theme.of(context).primaryColor)),
                  onPressed: () {
                    authBloc.add(AppLoaded());
                  },
                )
              ],
            ));
          }
          // return splash screen
          return Center(
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          );
        },
      )),
    );
  }
}

class _AuthForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = RepositoryProvider.of<AuthenticationService>(context);
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);

    return Container(
      alignment: Alignment.center,
      child: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(authBloc, authService),
        child: _SignInForm(),
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<_SignInForm>
    with TickerProviderStateMixin {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  bool _autoValidate = false;
  bool failedLogin = false;

  AnimationController _loginButtonController;
  var animationStatus = 0;
  Timer _timer;

  bool isLoading = true;
  String errorText = "";
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  @override
  void initState() {
    super.initState();
    CheckNetwork().checkConnection(context);
    _loginButtonController = new AnimationController(
        duration: new Duration(milliseconds: 3000), vsync: this);
  }

  @override
  void dispose() {
    if (CheckNetwork().listener != null) {
      CheckNetwork().listener.cancel();
    }
    _loginButtonController.dispose();
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      await _loginButtonController.forward();
      await _loginButtonController.reverse();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.4;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          _analyticsService.logLoginKO();
          setState(() {
            AppPrefs().removeUserCredentials();
            failedLogin = true;
          });
          if (!CheckNetwork().isConnected) {
            setState(() {
              errorText = txt('error_network');
            });
          } else {
            setState(() {
              errorText = txt('error_login');
            });
          }
          return ErrorDialogBox(
              txt('title_error_popup'), errorText, txt('btn_ok_dialog'));
        }
        if (state is LoginSuccess) {
          _analyticsService.logLoginOK();
          setState(() {
            failedLogin = false;
            // AppPrefs().setUserCredentials(
            //     _emailController.text, _passwordController.text);
          });
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          if (state is LoginLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is LoginFailure) {
            return ErrorDialogBox(
                txt('title_error_popup'), errorText, txt('btn_ok_dialog'));
          }

          return (new Scaffold(
            body: new Container(
                decoration: new BoxDecoration(
                  image: backgroundImage,
                ),
                child: new Container(
                    decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                      colors: <Color>[
                        const Color.fromRGBO(255, 255, 255, 0.95),
                        colors.soft_indigo,
                      ],
                      stops: [0.7, 1.0],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.0, 1.0),
                    )),
                    child: new ListView(
                      padding: EdgeInsets.only(top: _height / 100),
                      children: <Widget>[
                        new Stack(
                          alignment: AlignmentDirectional.bottomCenter,
                          children: <Widget>[
                            new Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                new Tick(image: tick),
                                Padding(
                                  padding:
                                      EdgeInsets.only(bottom: _height * 0.005),
                                  child: new FormContainer(
                                      _key,
                                      _emailController,
                                      _passwordController,
                                      _autoValidate),
                                )
                              ],
                            ),
                            animationStatus == 0 ||
                                    (_key.currentState != null &&
                                        !_key.currentState.validate()) ||
                                    failedLogin
                                ? new InkWell(
                                    onTap: () {
                                      setState(() {
                                        animationStatus = 1;
                                        failedLogin = false;
                                      });
                                      _playAnimation();
                                    },
                                    child: new SignIn())
                                : new StaggerAnimation(
                                    buttonController:
                                        _loginButtonController.view,
                                    analyticsService: _analyticsService,
                                    globalKey: _key,
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                    state: state,
                                  ),
                          ],
                        ),
                      ],
                    ))),
          ));
        },
      ),
    );
  }

  void _showError(String error) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(error),
      backgroundColor: Theme.of(context).errorColor,
    ));
  }
}
