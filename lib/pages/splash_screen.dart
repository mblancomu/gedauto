import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

import 'home_page.dart';

class Splash extends StatefulWidget {
  final Account user;

  Splash({Key key, this.user}) : super(key: key);

  @override
  VideoState createState() => VideoState(user: user);
}

class VideoState extends State<Splash> with SingleTickerProviderStateMixin {
  final Account user;

  VideoState({Key key, this.user});
  var _visible = true;

  AnimationController animationController;
  Animation<double> animation;

  startTime() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => HomePage(user: user)));
  }

  @override
  void initState() {
    super.initState();

    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 1));
    animation =
        new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset(
                'assets/logotipo_grupo_gedauto.png',
                width: animation.value * displayWidth(context) / 1.5,
                height: animation.value * displayHeight(context) / 2,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
