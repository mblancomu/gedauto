import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:gedauto/blocs/blocs.dart';
import 'package:gedauto/blocs/products/products_bloc.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/models.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/pages/products_page.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/products_service.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:gedauto/widgets/empty_list.dart';
import 'package:gedauto/widgets/error_dialog_box.dart';
import 'package:gedauto/widgets/loading.dart';
import 'package:gedauto/widgets/promotion_row.dart';
import 'package:scroll_bottom_navigation_bar/scroll_bottom_navigation_bar.dart';

class PromotionTab extends StatelessWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  PromotionTab(this.account, this._scrollBottomBarController);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: BlocProvider<ProductsBloc>(
        create: (context) => ProductsBloc(ProductsServiceImpl()),
        child: _PromotionsList(account, _scrollBottomBarController),
      ),
    );
  }
}

class _PromotionsList extends StatefulWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  _PromotionsList(this.account, this._scrollBottomBarController);

  @override
  State<StatefulWidget> createState() =>
      _PromotionTabState(account, _scrollBottomBarController);
}

class _PromotionTabState extends State<_PromotionsList> {
  ProductsBloc _productsBloc;
  Account account;
  ScrollController _scrollBottomBarController;

  _PromotionTabState(this.account, this._scrollBottomBarController);

  @override
  Widget build(BuildContext context) {
    _productsBloc = BlocProvider.of<ProductsBloc>(context);
    _loadData(_productsBloc, account.email, account.password);
    return Scaffold(
      body: Container(
        color: colors.soft_indigo,
        child: _body(),
      ),
    );
  }

  _body() {
    return Column(
      children: [
        BlocBuilder<ProductsBloc, ProductsState>(
            builder: (BuildContext context, ProductsState state) {
          if (state is PromotionsSuccess) {
            Product product = state.promotions;
            List<ProductItem> promotions = product.listado;
            return _animatedList(promotions);
          }
          if (state is PromotionsEmpty) {
            return EmptyList(txt('empty_promos_list'));
          }
          if (state is ProductsFailure) {
            return ErrorDialogBox(txt('title_error_popup'),
                txt('error_load_promotions'), txt('btn_ok_dialog'));
          }
          return Loading();
        }),
      ],
    );
  }

  Widget _animatedList(List<ProductItem> promotions) {
    return Expanded(
      child: AnimationLimiter(
        child: RefreshIndicator(
            onRefresh: () async {
              _loadData(_productsBloc, account.email, account.password);
            },
            child: NotificationListener(
              // ignore: missing_return
              onNotification: (t) {
                _scrollBottomBarController.bottomNavigationBar.setPinState(
                    isPinedBottomBar(
                        promotions.length, displayHeight(context)));
              },
              child: ListView.builder(
                  padding: new EdgeInsets.only(
                      top: 0, left: 0, right: 0, bottom: 20),
                  controller: _scrollBottomBarController,
                  itemCount: promotions.length,
                  itemBuilder: (context, index) {
                    return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 1000),
                        child: SlideAnimation(
                          verticalOffset: 50.0,
                          child: FadeInAnimation(
                            child: PromotionRow(
                              promotion: promotions[index],
                            ),
                          ),
                        ));
                  }),
            )),
      ),
    );
  }

  Future<void> _loadData(
      ProductsBloc _productsBloc, String email, String password) async {
    setState(() {
      if ((email != null && email.isNotEmpty) &&
          (password != null && password.isNotEmpty)) {
        _productsBloc
            .add(GetPromotionsOnHomePage(email: email, password: password));
      }
    });
  }
}
