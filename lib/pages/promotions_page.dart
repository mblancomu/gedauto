import 'package:flutter/material.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/pages/promotion_tab.dart';
import 'package:gedauto/resources/colors.dart';

import 'discount_tab.dart';

class PromotionsPage extends StatefulWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  PromotionsPage(this.account, this._scrollBottomBarController);

  @override
  _PromotionsPageState createState() =>
      _PromotionsPageState(account, _scrollBottomBarController);
}

class _PromotionsPageState extends State<PromotionsPage>
    with TickerProviderStateMixin {
  bool isSmall = false;
  TabController _tabController;
  Account account;
  ScrollController _scrollBottomBarController;

  _PromotionsPageState(this.account, this._scrollBottomBarController);

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {});
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(txt('page_promos')),
          backgroundColor: colors.primary,
          bottom: TabBar(
              labelStyle: TextStyle(fontSize: 20.0),
              unselectedLabelStyle: TextStyle(fontSize: 16.0),
              controller: _tabController,
              indicatorColor: colors.accent,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(
                  text: txt('tab_discounts'),
                ),
                Tab(
                  text: txt('tab_promotions'),
                )
              ]),
        ),
        body: TabBarView(
          children: [
            DiscountTab(account, _scrollBottomBarController),
            PromotionTab(account, _scrollBottomBarController),
          ],
          controller: _tabController,
        ));
  }
}
