import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/locator.dart';
import 'package:gedauto/pages/splash_screen.dart';
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/widgets/error_dialog_box.dart';

import '../blocs/blocs.dart';
import '../services/services.dart';

class SilentLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = RepositoryProvider.of<AuthenticationService>(context);
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);

    return Container(
      alignment: Alignment.center,
      child: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(authBloc, authService),
        child: _SignInForm(),
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<_SignInForm> {
  bool isLoading = true;
  String email;
  String password;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  @override
  void initState() {
    super.initState();

    AppPrefs().getUserEmail().then((emailResult) {
      if (emailResult != null) {
        setState(() {
          email = emailResult;
        });
      }
    });

    AppPrefs().getUserPassword().then((passwordResult) {
      if (passwordResult != null) {
        setState(() {
          password = passwordResult;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _loginBloc = BlocProvider.of<LoginBloc>(context);
    if (email != null && password != null) {
      _loginBloc
          .add(LoginInWithEmailButtonPressed(email: email, password: password));
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          _analyticsService.logLoginKO();
          AppPrefs().removeUserCredentials();
        } else if (state is LoginSuccess) {
          setState(() {
            AppPrefs().setUserCredentials(email, password);
          });
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          if (state is LoginFailure) {
            return ErrorDialogBox(
                'Error', 'Se ha producido un error de Login', 'LOGIN');
          }

          if (state is LoginSuccess) {
            return Splash(user: state.user);
          }

          return Container(
            color: Colors.white,
            child: Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.red,
            )),
          );
        },
      ),
    );
  }
}
