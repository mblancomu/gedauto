import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/blocs/account/account.dart';
import 'package:gedauto/blocs/account/account_bloc.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/prefs/globals.dart' as globals;
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/analytics_service.dart';
import 'package:gedauto/services/user_service.dart';
import 'package:gedauto/utils/constants.dart';
import 'package:gedauto/utils/home_styles.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:gedauto/widgets/login/logout_button.dart';
import 'package:gedauto/widgets/profile/fade_container.dart';
import 'package:gedauto/widgets/profile/home_button_animation.dart';
import 'package:gedauto/widgets/profile/profile.dart';
import 'package:gedauto/widgets/promos_dialog_box.dart';
import 'package:gedauto/widgets/user_header.dart';

import '../locator.dart';

class UserPage extends StatelessWidget {
  Account account;

  UserPage(this.account);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: BlocProvider<AccountBloc>(
        create: (context) => AccountBloc(AccountServiceImpl()),
        child: _UserAccount(account),
      ),
    );
  }
}

class _UserAccount extends StatefulWidget {
  Account account;

  _UserAccount(this.account);

  @override
  State<StatefulWidget> createState() => _UserPageState(account);
}

class _UserPageState extends State<_UserAccount> with TickerProviderStateMixin {
  Animation<double> containerGrowAnimation;
  AnimationController _screenController;
  AnimationController _buttonController;
  Animation<double> buttonGrowAnimation;
  Animation<Alignment> buttonSwingAnimation;
  Animation<Color> fadeScreenAnimation;
  var animateStatus = 0;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  Account account;
  String email;
  String password;

  _UserPageState(this.account);

  @override
  void initState() {
    super.initState();
    _screenController = new AnimationController(
        duration: new Duration(milliseconds: 2000), vsync: this);
    _buttonController = new AnimationController(
        duration: new Duration(milliseconds: 1500), vsync: this);

    fadeScreenAnimation = new ColorTween(
      begin: colors.primary,
      end: const Color.fromRGBO(247, 64, 106, 0.0),
    ).animate(
      new CurvedAnimation(
        parent: _screenController,
        curve: Curves.ease,
      ),
    );
    containerGrowAnimation = new CurvedAnimation(
      parent: _screenController,
      curve: Curves.easeIn,
    );

    buttonGrowAnimation = new CurvedAnimation(
      parent: _screenController,
      curve: Curves.easeOut,
    );
    containerGrowAnimation.addListener(() {
      this.setState(() {});
    });
    containerGrowAnimation.addStatusListener((AnimationStatus status) {});

    buttonSwingAnimation = new AlignmentTween(
      begin: Alignment.topCenter,
      end: Alignment.bottomRight,
    ).animate(
      new CurvedAnimation(
        parent: _screenController,
        curve: new Interval(
          0.225,
          0.600,
          curve: Curves.ease,
        ),
      ),
    );

    _screenController.forward();

    _promosCheck();

    AppPrefs().getUserEmail().then((emailResult) {
      if (emailResult != null) {
        setState(() {
          email = emailResult;
        });
      }
    });

    AppPrefs().getUserPassword().then((passwordResult) {
      if (passwordResult != null) {
        setState(() {
          password = passwordResult;
        });
      }
    });
  }

  _promosCheck() {
    if (account.notificacion != null &&
        account.notificacion.isNotEmpty &&
        !globals.isConsumedPromo) {
      existPromos(account.notificacion, context);
      globals.isConsumedPromo = true;
    }
  }

  @override
  void dispose() {
    _screenController.dispose();
    _buttonController.dispose();
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      await _buttonController.forward();
    } on TickerCanceled {}
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.3;
    Size screenSize = MediaQuery.of(context).size;

    return (new WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: Scaffold(
          body: Container(
            width: screenSize.width,
            height: screenSize.height,
            color: colors.white,
            child: new Stack(
              //alignment: buttonSwingAnimation.value,
              alignment: Alignment.bottomRight,
              children: <Widget>[
                new ListView(
                  shrinkWrap: _screenController.value < 1 ? false : true,
                  padding: const EdgeInsets.all(0.0),
                  children: <Widget>[
                    new ImageBackground(
                      backgroundImage: backgroundImage,
                      containerGrowAnimation: containerGrowAnimation,
                      profileImage: profileImage,
                    ),
                    new Profile(
                      account: account,
                    ),
                    //new Calender(),
                  ],
                ),
                new FadeBox(
                  fadeScreenAnimation: fadeScreenAnimation,
                  containerGrowAnimation: containerGrowAnimation,
                ),
                animateStatus == 0
                    ? new Padding(
                        padding: new EdgeInsets.all(20.0),
                        child: new InkWell(
                            splashColor: Colors.white,
                            highlightColor: Colors.white,
                            onTap: () {
                              setState(() {
                                animateStatus = 1;
                              });
                              _playAnimation();
                            },
                            child: new AddButton(
                              buttonGrowAnimation: buttonGrowAnimation,
                            )))
                    : new StaggerAnimation(
                        buttonController: _buttonController.view,
                        analyticsService: _analyticsService,
                      ),
              ],
            ),
          ),
        )));
  }
}

Widget buildLoading() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

Widget buildErrorUi(String message, BuildContext context) {
  return Center(
    child: Padding(
      padding: EdgeInsets.all(displayWidth(context) / 50),
      child: Text(
        message,
        style: TextStyle(
            color: Colors.red,
            fontSize: displayWidth(context) / Constants.unselectedIconSize),
      ),
    ),
  );
}

existPromos(String promo, BuildContext context) {
  WidgetsBinding.instance.addPostFrameCallback((_) async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) => PromosDialogBox(
              title: txt('title_new_promotion'),
              descriptions: txt('message_new_promo_intro') +
                  promo +
                  txt('message_new_promo_body') +
                  promo,
              text: txt('btn_ok_dialog'),
            ));
  });
}
