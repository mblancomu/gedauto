import 'package:flutter/material.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/pages/pages.dart';
import 'package:gedauto/pages/products_page.dart';
import 'package:gedauto/pages/user_page.dart';
import 'package:gedauto/prefs/globals.dart' as globals;
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/resources/gedauto_icons_icons.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:scroll_bottom_navigation_bar/scroll_bottom_navigation_bar.dart';

import '../models/models.dart';

class HomePage extends StatefulWidget {
  final Account user;

  HomePage({Key key, this.user}) : super(key: key);

  @override
  HomePageState createState() => HomePageState(user);
}

class HomePageState extends State<HomePage> {
  final Account user;
  int selectedIndex = 0;
  String titleBar = txt('page_user');
  ScrollController controller;

  HomePageState(this.user);

  @override
  void initState() {
    super.initState();
    controller = ScrollController();

    globals.isConsumedPromo = false;
  }

  @override
  void dispose() {
    controller.bottomNavigationBar.dispose();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screens = <Widget>[
      UserPage(user),
      ProductsPage(user, controller),
      PromotionsPage(user, controller),
    ];

    return Scaffold(
        body: Snap(
          controller: controller.bottomNavigationBar,
          child: ValueListenableBuilder<int>(
              valueListenable: controller.bottomNavigationBar.tabNotifier,
              builder: (context, value, child) => screens[value]),
        ),
        bottomNavigationBar: SafeArea(
          bottom: true,
          child: ScrollBottomNavigationBar(
            controller: controller,
            backgroundColor: colors.primary,
            selectedItemColor: null,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                    GedautoIcons.user_alt,
                    color: colors.light_grey,
                    size: displayWidth(context) * 0.05,
                  ),
                  activeIcon: Icon(
                    GedautoIcons.user_alt,
                    color: colors.white,
                    size: displayWidth(context) * 0.055,
                  ),
                  label: txt('page_user')),
              BottomNavigationBarItem(
                  icon: Icon(
                    GedautoIcons.basket_1,
                    color: colors.light_grey,
                    size: displayWidth(context) * 0.05,
                  ),
                  activeIcon: Icon(
                    GedautoIcons.basket_1,
                    color: colors.white,
                    size: displayWidth(context) * 0.055,
                  ),
                  label: txt('page_shopping')),
              BottomNavigationBarItem(
                  icon: Icon(
                    GedautoIcons.tags_1,
                    color: colors.light_grey,
                    size: displayWidth(context) * 0.05,
                  ),
                  activeIcon: Icon(
                    GedautoIcons.tags_1,
                    color: colors.white,
                    size: displayWidth(context) * 0.055,
                  ),
                  label: txt('page_promos')),
            ],
            fixedColor: null,
            unselectedItemColor: colors.light_grey,
          ),
        ));
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
      switch (index) {
        case 0:
          {
            titleBar = txt('page_user');
          }
          break;
        case 1:
          {
            titleBar = txt('page_shopping');
          }
          break;
        case 2:
          {
            titleBar = txt('page_promos');
          }
          break;
      }
    });
  }
}
