import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:gedauto/blocs/products/products_bloc.dart';
import 'package:gedauto/blocs/products/products_event.dart';
import 'package:gedauto/blocs/products/products_state.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/models/product.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/pages/products_page.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/products_service.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:gedauto/widgets/discount_row.dart';
import 'package:gedauto/widgets/empty_list.dart';
import 'package:gedauto/widgets/error_dialog_box.dart';
import 'package:gedauto/widgets/loading.dart';
import 'package:scroll_bottom_navigation_bar/scroll_bottom_navigation_bar.dart';

class DiscountTab extends StatelessWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  DiscountTab(this.account, this._scrollBottomBarController);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: BlocProvider<ProductsBloc>(
        create: (context) => ProductsBloc(ProductsServiceImpl()),
        child: _DiscountsList(account, _scrollBottomBarController),
      ),
    );
  }
}

class _DiscountsList extends StatefulWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  _DiscountsList(this.account, this._scrollBottomBarController);

  @override
  State<StatefulWidget> createState() =>
      _DiscountTabState(account, _scrollBottomBarController);
}

class _DiscountTabState extends State<_DiscountsList> {
  Account account;
  ProductsBloc _productsBloc;
  ScrollController _scrollBottomBarController;

  _DiscountTabState(this.account, this._scrollBottomBarController);

  @override
  Widget build(BuildContext context) {
    _productsBloc = BlocProvider.of<ProductsBloc>(context);
    _loadData(_productsBloc, account.email, account.password);
    return Scaffold(
      body: Container(
        color: colors.soft_indigo,
        child: _body(),
      ),
    );
  }

  _body() {
    return Column(
      children: [
        BlocBuilder<ProductsBloc, ProductsState>(
            builder: (BuildContext context, ProductsState state) {
          if (state is DiscountsSuccess) {
            Product product = state.discounts;
            List<ProductItem> discounts = product.listado;
            return _animatedList(discounts);
          }
          if (state is DiscountsEmpty) {
            return EmptyList(txt('empty_discounts_list'));
          }
          if (state is ProductsFailure) {
            return ErrorDialogBox(txt('title_error_popup'),
                txt('error_load_discounts'), txt('btn_ok_dialog'));
          }
          return Loading();
        }),
      ],
    );
  }

  Widget _animatedList(List<ProductItem> discounts) {
    return Expanded(
      child: AnimationLimiter(
        child: RefreshIndicator(
            onRefresh: () async {
              _loadData(_productsBloc, account.email, account.password);
            },
            child: NotificationListener(
              // ignore: missing_return
              onNotification: (t) {
                _scrollBottomBarController.bottomNavigationBar.setPinState(
                    isPinedBottomBar(discounts.length, displayHeight(context)));
              },
              child: ListView.builder(
                  padding: new EdgeInsets.only(
                      top: 0, left: 0, right: 0, bottom: 20),
                  controller: _scrollBottomBarController,
                  itemCount: discounts.length,
                  itemBuilder: (context, index) {
                    return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 1000),
                        child: SlideAnimation(
                          verticalOffset: 50.0,
                          child: FadeInAnimation(
                            child: DiscountRow(
                              discount: discounts[index],
                            ),
                          ),
                        ));
                  }),
            )),
      ),
    );
  }

  Future<void> _loadData(
      ProductsBloc _productsBloc, String email, String password) async {
    setState(() {
      if ((email != null && email.isNotEmpty) &&
          (password != null && password.isNotEmpty)) {
        _productsBloc
            .add(GetDiscountsOnHomePage(email: email, password: password));
      }
    });
  }
}
