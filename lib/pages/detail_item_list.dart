import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/resources/extra_icons.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:url_launcher/url_launcher.dart';

class Detail extends StatefulWidget {
  ProductItem productItem;
  Color backgroundColor;

  Detail({this.productItem, this.backgroundColor});

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  //double _screenWidth = window.physicalSize.width / window.devicePixelRatio;
  double _screenWidth;
  double _iconSize;
  Color _color;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _screenWidth = MediaQuery.of(context).size.width;
  }

  @override
  void initState() {
    super.initState();
    _iconSize = 0;
    _color = widget.backgroundColor;
  }

  void change() {
    setState(() {
      _iconSize = 20;
      _color = Colors.greenAccent;
    });
  }

  void reverse() {
    setState(() {
      _iconSize = 0;
      _color = widget.backgroundColor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: colors.soft_indigo,
        appBar: AppBar(
          backgroundColor: Color(0x66000000),
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                GestureDetector(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    color: widget.backgroundColor,
                    elevation: 2,
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Hero(
                            tag: 'image' +
                                _createTagForHero(widget.productItem.titulo,
                                    widget.productItem.codigo),
                            child: Material(
                              color: Colors.transparent,
                              child: SizedBox(
                                width: displayWidth(context),
                                height: heightPlaceHolder(context,
                                    widget.productItem.h, widget.productItem.w),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(10.0),
                                      bottom: Radius.zero),
                                  child: FadeInImage.assetNetwork(
                                    placeholder:
                                        "assets/placeholder_detail.png",
                                    image: widget.productItem.zoom ?? "",
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Hero(
                              tag: 'title' +
                                  _createTagForHero(widget.productItem.titulo,
                                      widget.productItem.codigo),
                              child: Material(
                                color: Colors.transparent,
                                child: SizedBox(
                                  width: _screenWidth,
                                  child: Padding(
                                    padding: new EdgeInsets.only(
                                        top: 8, left: 10, right: 10, bottom: 0),
                                    child: Text('${widget.productItem.titulo}',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                            fontSize:
                                                displayWidth(context) * 0.07)),
                                  ),
                                ),
                              )),
                          Hero(
                              tag: 'date' +
                                  _createTagForHero(
                                      widget.productItem.fechaPublicacion,
                                      widget.productItem.codigo),
                              child: Material(
                                color: Colors.transparent,
                                child: SizedBox(
                                  width: _screenWidth,
                                  child: Padding(
                                    padding: new EdgeInsets.only(
                                        top: 0,
                                        left: 10,
                                        right: 10,
                                        bottom: 10),
                                    child: Text(
                                        'Cod: ${widget.productItem.codigo}',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.black54,
                                            fontSize:
                                                displayWidth(context) * 0.04)),
                                  ),
                                ),
                              )),
                          Hero(
                              tag: 'subtitle' +
                                  _createTagForHero(widget.productItem.titulo,
                                      widget.productItem.codigo),
                              child: Material(
                                  color: Colors.transparent,
                                  child: SizedBox(
                                    width: _screenWidth,
                                    child: Padding(
                                      padding: new EdgeInsets.only(
                                          top: 8,
                                          left: 10,
                                          right: 10,
                                          bottom: 30),
                                      child: Text(
                                        widget.productItem.descripcion ?? "",
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color:
                                                Colors.white.withOpacity(0.9),
                                            fontSize:
                                                displayWidth(context) * 0.04),
                                      ),
                                    ),
                                  ))),
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Visibility(
                    visible: _isVisibleButton(widget.productItem.enlaceUrl,
                        widget.productItem.enlaceTexto),
                    child: AnimatedContainer(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.8),
                            spreadRadius: 1,
                            blurRadius: 1,
                            offset: Offset(0, 1), // changes position of shadow
                          ),
                        ],
                      ),
                      duration: const Duration(milliseconds: 800),
                      child: _buttonPreview(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buttonPreview() {
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      minimumSize: Size(_screenWidth / 2, 50),
      backgroundColor: _color,
      textStyle: TextStyle(color: Colors.white),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: _color)),
      padding: EdgeInsets.all(8.0),
    );
    return TextButton.icon(
        onPressed: () {
          change();
          Timer(Duration(seconds: 1), () {
            reverse();
          });
          _launchURL(widget.productItem.enlaceUrl);
        },
        style: flatButtonStyle,
        icon: Icon(
          ExtraIcons.ok,
          color: Colors.white,
          size: _iconSize,
        ),
        label: Text(
          (_isVisibleButton(
                  widget.productItem.enlaceUrl, widget.productItem.enlaceTexto))
              ? widget.productItem.enlaceTexto.toUpperCase()
              : "",
          style: TextStyle(fontSize: 14.0, color: Colors.white),
          overflow: TextOverflow.ellipsis,
        ));
  }
}

String _createTagForHero(String section, String code) {
  var tag = '$section$code';
  return tag;
}

double heightPlaceHolder(BuildContext context, int height, int width) {
  if (height != null && height > 0 && width != null && width > 0) {
    return (height / width) * displayWidth(context);
  }
  return 1.44 * displayWidth(context);
}

bool _isVisibleButton(String url, String text) {
  if (url == null || text == null || url.isEmpty || text.isEmpty) {
    return false;
  }
  return true;
}

_launchURL(String url) async {
  final uri = Uri.parse(url);
  if (await canLaunchUrl(uri)) {
    await launchUrl(uri, mode: LaunchMode.inAppWebView);
  } else {
    throw 'Could not launch $url';
  }
}
