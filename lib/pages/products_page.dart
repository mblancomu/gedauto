import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:gedauto/blocs/blocs.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/models/product.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/products_service.dart';
import 'package:gedauto/utils/sizes_helpers.dart';
import 'package:gedauto/widgets/empty_list.dart';
import 'package:gedauto/widgets/error_dialog_box.dart';
import 'package:gedauto/widgets/loading.dart';
import 'package:gedauto/widgets/products_row.dart';
import 'package:scroll_bottom_navigation_bar/scroll_bottom_navigation_bar.dart';

class ProductsPage extends StatelessWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  ProductsPage(this.account, this._scrollBottomBarController);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: BlocProvider<ProductsBloc>(
        create: (context) => ProductsBloc(ProductsServiceImpl()),
        child: _ProductList(account, _scrollBottomBarController),
      ),
    );
  }
}

class _ProductList extends StatefulWidget {
  Account account;
  ScrollController _scrollBottomBarController;

  _ProductList(this.account, this._scrollBottomBarController);

  @override
  State<StatefulWidget> createState() =>
      _ProductsListState(account, _scrollBottomBarController);
}

class _ProductsListState extends State<_ProductList> {
  ProductsBloc _productsBloc;
  ScrollController _scrollBottomBarController;
  Account account;

  _ProductsListState(this.account, this._scrollBottomBarController);

  @override
  void initState() {
    super.initState();

    _scrollBottomBarController.initialScrollOffset;
  }

  @override
  Widget build(BuildContext context) {
    _productsBloc = BlocProvider.of<ProductsBloc>(context);
    _loadData(_productsBloc, account.email, account.password);
    return Scaffold(
      appBar: AppBar(
        title: Text(txt('page_shopping')),
        backgroundColor: colors.primary,
      ),
      body: Container(
        color: colors.soft_indigo,
        child: _body(),
      ),
    );
  }

  _body() {
    return Column(
      children: [
        BlocBuilder<ProductsBloc, ProductsState>(
            builder: (BuildContext context, ProductsState state) {
          if (state is ProductsSuccess) {
            Product product = state.products;
            List<ProductItem> products = product.listado;
            return _animatedList(products);
          }
          if (state is ProductsEmpty) {
            return EmptyList(txt('empty_products_list'));
          }
          if (state is ProductsFailure) {
            return ErrorDialogBox(txt('title_error_popup'),
                txt('error_load_promotions'), txt('btn_ok_dialog'));
          }
          return Loading();
        }),
      ],
    );
  }

  Widget _animatedList(List<ProductItem> products) {
    return Expanded(
      child: AnimationLimiter(
        child: RefreshIndicator(
            onRefresh: () async {
              _loadData(_productsBloc, account.email, account.password);
            },
            child: NotificationListener(
              // ignore: missing_return
              onNotification: (t) {
                _scrollBottomBarController.bottomNavigationBar.setPinState(
                    isPinedBottomBar(products.length, displayHeight(context)));
              },
              child: ListView.builder(
                  padding: new EdgeInsets.only(
                      top: 0, left: 0, right: 0, bottom: 20),
                  controller: _scrollBottomBarController,
                  itemCount: products.length,
                  itemBuilder: (context, index) {
                    return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 1000),
                        child: SlideAnimation(
                          verticalOffset: 50.0,
                          child: FadeInAnimation(
                            child: ProductsRow(
                              product: products[index],
                            ),
                          ),
                        ));
                  }),
            )),
      ),
    );
  }

  Future<void> _loadData(
      ProductsBloc _productsBloc, String email, String password) async {
    setState(() {
      if ((email != null && email.isNotEmpty) &&
          (password != null && password.isNotEmpty)) {
        _productsBloc
            .add(GetProductsOnHomePage(email: email, password: password));
      }
    });
  }
}

bool isPinedBottomBar(int itemsList, double heightDevice) {
  var height5Items = (180.0 * 3) + 20;
  if (itemsList > 3) {
    if (height5Items < heightDevice) {
      return true;
    }
    return false;
  }
  return true;
}
