class DateConvertUtils {
  String convertDateTime(String date) {
    if (date != null) {
      DateTime dateItem = DateTime.parse(date);
      return dateItem.day.toString() +
          "-" +
          dateItem.month.toString() +
          "-" +
          dateItem.year.toString();
    }
    return "";
  }
}
