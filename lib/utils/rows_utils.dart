import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/pages/detail_item_list.dart';

class RowUtils {
  launchDetail(BuildContext context, ProductItem product, MaterialColor color) {
    if (product != null) {
      Navigator.of(context).push(
        PageRouteBuilder(
          maintainState: false,
          fullscreenDialog: true,
          transitionDuration: Duration(milliseconds: 2000),
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return Detail(
              productItem: product,
              backgroundColor: color,
            );
          },
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return FadeTransition(
              opacity: animation,
              // CurvedAnimation(parent: animation, curve: Curves.elasticInOut),
              child: child,
            );
          },
        ),
      );
    }
  }

  String createTagForHero(String section, String code) {
    var tag = '$section$code';
    return tag;
  }
}
