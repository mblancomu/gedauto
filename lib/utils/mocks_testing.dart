import '../models/account.dart';

class MocksTesting {
  static Account createFakeAccount() {
    return Account(
        nombre: "Manuel Blanco",
        result: "Algo bueno",
        codigoVip: "654654654646",
        nif: "65765765d",
        tarjetaBloqueada: 0,
        notificacion: "",
        password: "123456",
        email: "manu@hola.es");
  }
}
