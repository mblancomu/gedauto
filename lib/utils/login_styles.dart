import 'package:flutter/material.dart';

DecorationImage backgroundImage = new DecorationImage(
  image: new ExactAssetImage('assets/concesionario.jpg'),
  fit: BoxFit.cover,
);

DecorationImage tick = new DecorationImage(
  image: new ExactAssetImage('assets/square_logo.png'),
  fit: BoxFit.cover,
);
