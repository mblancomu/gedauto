class Constants {
  Constants._();

  static const double padding = 20;
  static const double avatarRadius = 45;

  static const double selectedIconSize = 16;
  static const double unselectedIconSize = 22;

  static const String _apiKey = 'PASTE YOUR API KEY HERE';

  static String get apiKey => _apiKey;
  static const String _prod_host = 'tarjetavip.gedautoapp.es';
  static const String _dev_host = 'gedautoapp.desarrollogbl.es';
  static const String _contextRoot = 'api/';

  static String get host => _prod_host;

  static String get contextRoot => _contextRoot;

  static const APP_STORE_URL =
      'https://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=YOUR-APP-ID&mt=8';
  static const PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=YOUR-APP-ID';
}
