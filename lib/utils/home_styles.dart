import 'package:flutter/material.dart';

DecorationImage backgroundImage = new DecorationImage(
  image: new ExactAssetImage('assets/concesionario_header.jpg'),
  fit: BoxFit.cover,
);

DecorationImage profileImage = new DecorationImage(
  image: new ExactAssetImage('assets/square_logo.png'),
  fit: BoxFit.cover,
);
