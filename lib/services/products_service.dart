import 'dart:async';

import 'package:gedauto/models/product.dart';
import 'package:gedauto/services/base_service.dart';

abstract class ProductsService extends BaseService {
  Future<Product> getProductsUser(String email, String password);

  Future<Product> getPromotionsUser(String email, String password);

  Future<Product> getDiscountsUser(String email, String password);
}

class ProductsServiceImpl extends ProductsService {
  @override
  Future<Product> getProductsUser(String email, String password) async {
    final results = await request(
        path: 'get-productos/',
        parameters: {},
        headers: {'username': email, 'password': password});

    return new Product.fromJson(results);
  }

  @override
  Future<Product> getDiscountsUser(String email, String password) async {
    final results = await request(
        path: 'get-descuentos/',
        parameters: {},
        headers: {'username': email, 'password': password});

    return new Product.fromJson(results);
  }

  @override
  Future<Product> getPromotionsUser(String email, String password) async {
    final results = await request(
        path: 'get-promociones/',
        parameters: {},
        headers: {'username': email, 'password': password});

    return new Product.fromJson(results);
  }
}
