import 'dart:async';

import 'package:gedauto/models/account.dart';
import 'package:gedauto/services/base_service.dart';

abstract class AccountService extends BaseService {
  Future<Account> getUserAccount(String email, String password);
}

class AccountServiceImpl extends AccountService {
  @override
  Future<Account> getUserAccount(String email, String password) async {
    final results = await request(
        path: 'usuario',
        parameters: {},
        headers: {'username': email, 'password': password});

    return new Account.fromJson(results, password);
  }
}
