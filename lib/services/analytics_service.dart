import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics.instance;

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  // User properties tells us what the user is
  Future setUserProperties({@required String userId, String userRole}) async {
    await _analytics.setUserId();
    await _analytics.setUserProperty(name: 'user_role', value: userRole);
    // property to indicate if it's a pro paying member
    // property that might tell us it's a regular poster, etc
  }

  Future logLogin() async {
    await _analytics.logLogin(loginMethod: 'email');
  }

  Future logLoginOK() async {
    await _analytics.logEvent(name: 'Login OK');
  }

  Future logLoginKO() async {
    await _analytics.logEvent(name: 'Login KO');
  }

  Future logLogOut() async {
    await _analytics.logEvent(name: 'Log Out');
  }

  Future logAutoLogin() async {
    await _analytics.logEvent(name: 'Auto Login');
  }

  Future logSignUp() async {
    await _analytics.logSignUp(signUpMethod: 'email');
  }

  Future logAccountOK() async {
    await _analytics.logEvent(name: 'Loaded Account OK');
  }

  Future logAccountKO() async {
    await _analytics.logEvent(name: 'Loaded Account KO');
  }

  Future logProductsOK() async {
    await _analytics.logEvent(name: 'Loaded Products OK');
  }

  Future logProductsKO() async {
    await _analytics.logEvent(name: 'Loaded Products KO');
  }

  Future logProductsEmpty() async {
    await _analytics.logEvent(name: 'Products List Empty');
  }

  Future logDiscountsOK() async {
    await _analytics.logEvent(name: 'Loaded Discounts OK');
  }

  Future logDiscountsKO() async {
    await _analytics.logEvent(name: 'Loaded Discounts KO');
  }

  Future logDiscountsEmpty() async {
    await _analytics.logEvent(name: 'Discounts List Empty');
  }

  Future logPromotionsOK() async {
    await _analytics.logEvent(name: 'Loaded Promotions OK');
  }

  Future logPromotionsKO() async {
    await _analytics.logEvent(name: 'Loaded Promotions KO');
  }

  Future logPromotionsEmpty() async {
    await _analytics.logEvent(name: 'Promotions List Empty');
  }

  Future logPostCreated({bool hasImage}) async {
    await _analytics.logEvent(
      name: 'create_post',
      parameters: {'has_image': hasImage},
    );
  }
}
