import 'dart:async';
import 'dart:convert' show json;
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/utils/constants.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class BaseService {
  Future<Map> request(
      {@required String path,
      Map<String, String> parameters,
      Map<String, String> headers}) async {
    String username = headers['username'];
    String password = headers['password'];
    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$username:$password'));
    String root = Constants.contextRoot;
    final uri = Uri.http(Constants.host, '$root$path', parameters);
    final results = await http
        .get(uri, headers: <String, String>{'Authorization': basicAuth});
    if (results.statusCode != 200)
      throw http.ClientException(txt('error_login'));

    // String fakeJson;
    // if (path == 'products') {
    //   fakeJson = await _loadProductsAsset();
    // } else {
    //   fakeJson = await _loadAccoutAsset();
    // }

    final jsonObject = json.decode(results.body);
    return jsonObject;
  }
}

Future<String> _loadProductsAsset() async {
  return await rootBundle.loadString('assets/products_fake.json');
}

Future<String> _loadAccoutAsset() async {
  return await rootBundle.loadString('assets/account_fake.json');
}
