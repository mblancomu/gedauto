import 'dart:async';

import 'package:gedauto/models/account.dart';
import 'package:gedauto/services/base_service.dart';

import '../models/models.dart';

abstract class AuthenticationService extends BaseService {
  Future<Account> getCurrentUser();
  Future<Account> signInWithEmailAndPassword(String email, String password);
  Future<void> signOut();
}

class FakeAuthenticationService extends AuthenticationService {
  @override
  Future<Account> getCurrentUser() async {
    return null; // return null for now
  }

  @override
  Future<Account> signInWithEmailAndPassword(
      String email, String password) async {
    final results = await request(
        path: 'get-usuario/',
        parameters: {},
        headers: {'username': email, 'password': password});

    return new Account.fromJson(results, password);
  }

  @override
  Future<void> signOut() {
    return null;
  }
}
