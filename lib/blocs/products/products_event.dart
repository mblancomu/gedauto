import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ProductsEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetProductsOnHomePage extends ProductsEvent {
  final String email;
  final String password;

  GetProductsOnHomePage({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}

class GetDiscountsOnHomePage extends ProductsEvent {
  final String email;
  final String password;

  GetDiscountsOnHomePage({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}

class GetPromotionsOnHomePage extends ProductsEvent {
  final String email;
  final String password;

  GetPromotionsOnHomePage({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}
