import 'package:equatable/equatable.dart';
import 'package:gedauto/models/product.dart';
import 'package:meta/meta.dart';

abstract class ProductsState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductsState {}

class ProductsLoading extends ProductsState {}

class ProductsSuccess extends ProductsState {
  final Product products;

  ProductsSuccess({@required this.products});

  @override
  // TODO: implement props
  List<Object> get props => [products];
}

class PromotionsSuccess extends ProductsState {
  final Product promotions;

  PromotionsSuccess({@required this.promotions});

  @override
  // TODO: implement props
  List<Object> get props => [promotions];
}

class DiscountsSuccess extends ProductsState {
  final Product discounts;

  DiscountsSuccess({@required this.discounts});

  @override
  // TODO: implement props
  List<Object> get props => [discounts];
}

class ProductsFailure extends ProductsState {
  final String error;

  ProductsFailure({@required this.error});

  @override
  List<Object> get props => [error];
}

class ProductsEmpty extends ProductsState {}

class PromotionsEmpty extends ProductsState {}

class DiscountsEmpty extends ProductsState {}
