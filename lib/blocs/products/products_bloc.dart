import 'package:bloc/bloc.dart';
import 'package:gedauto/blocs/products/products_event.dart';
import 'package:gedauto/blocs/products/products_state.dart';
import 'package:gedauto/exceptions/products_exception.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/services/analytics_service.dart';

import '../../locator.dart';
import '../../services/services.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsService _productsService;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  ProductsBloc(ProductsService productsService)
      : assert(productsService != null),
        _productsService = productsService,
        super(ProductsInitial());

  @override
  Stream<ProductsState> mapEventToState(ProductsEvent event) async* {
    if (event is GetProductsOnHomePage) {
      yield* _mapGetProductsToState(event);
    }
    if (event is GetPromotionsOnHomePage) {
      yield* _mapGetPromotionsToState(event);
    }
    if (event is GetDiscountsOnHomePage) {
      yield* _mapGetDiscountsToState(event);
    }
  }

  Stream<ProductsState> _mapGetProductsToState(
      GetProductsOnHomePage event) async* {
    yield ProductsLoading();
    try {
      final products =
          await _productsService.getProductsUser(event.email, event.password);
      if (products != null && products.result == 'OK') {
        await _analyticsService.logProductsOK();
        if (products.listado.isEmpty) {
          yield ProductsEmpty();
        } else {
          yield ProductsSuccess(products: products);
        }
      } else {
        yield ProductsFailure(error: txt('error_load_products'));
        await _analyticsService.logProductsKO();
      }
    } on ProductsException catch (e) {
      yield ProductsFailure(error: txt('error_load_products'));
    } catch (err) {
      await _analyticsService.logProductsKO();
      yield ProductsFailure(error: txt('error_load_products'));
    }
  }

  Stream<ProductsState> _mapGetPromotionsToState(
      GetPromotionsOnHomePage event) async* {
    yield ProductsLoading();
    try {
      final promotions =
          await _productsService.getPromotionsUser(event.email, event.password);
      if (promotions != null && promotions.result == 'OK') {
        await _analyticsService.logPromotionsOK();
        if (promotions.listado.isEmpty) {
          yield PromotionsEmpty();
        } else {
          yield PromotionsSuccess(promotions: promotions);
        }
      } else {
        yield ProductsFailure(error: txt('error_load_promotions'));
        await _analyticsService.logPromotionsKO();
      }
    } on ProductsException catch (e) {
      yield ProductsFailure(error: txt('error_load_promotions'));
    } catch (err) {
      await _analyticsService.logPromotionsKO();
      yield ProductsFailure(error: txt('error_load_promotions'));
    }
  }

  Stream<ProductsState> _mapGetDiscountsToState(
      GetDiscountsOnHomePage event) async* {
    yield ProductsLoading();
    try {
      final discounts =
          await _productsService.getDiscountsUser(event.email, event.password);
      if (discounts != null && discounts.result == 'OK') {
        await _analyticsService.logDiscountsOK();
        if (discounts.listado.isEmpty) {
          yield DiscountsEmpty();
        } else {
          yield DiscountsSuccess(discounts: discounts);
        }
      } else {
        yield ProductsFailure(error: txt('error_load_discounts'));
        await _analyticsService.logDiscountsKO();
      }
    } on ProductsException catch (e) {
      yield ProductsFailure(error: txt('error_load_discounts'));
    } catch (err) {
      await _analyticsService.logDiscountsKO();
      yield ProductsFailure(error: txt('error_load_discounts'));
    }
  }
}
