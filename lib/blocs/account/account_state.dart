import 'package:equatable/equatable.dart';
import 'package:gedauto/models/account.dart';
import 'package:meta/meta.dart';

abstract class AccountState extends Equatable {
  @override
  List<Object> get props => [];
}

class AccountInitial extends AccountState {}

class AccountLoading extends AccountState {}

class AccountSuccess extends AccountState {
  Account account;

  AccountSuccess({@required this.account});

  @override
  // TODO: implement props
  Object get prop => account;
}

class AccountFailure extends AccountState {
  final String error;

  AccountFailure({@required this.error});

  @override
  List<Object> get props => [error];
}
