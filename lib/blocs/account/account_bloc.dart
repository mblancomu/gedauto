import 'package:bloc/bloc.dart';
import 'package:gedauto/blocs/account/account_event.dart';
import 'package:gedauto/blocs/account/account_state.dart';
import 'package:gedauto/exceptions/account_exception.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/services/analytics_service.dart';

import '../../locator.dart';
import '../../services/services.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  final AccountService _accountService;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  AccountBloc(AccountService accountService)
      : assert(accountService != null),
        _accountService = accountService,
        super(AccountInitial());

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is GetAccountOnHomePage) {
      yield* _mapGetAccountToState(event);
    }
  }

  Stream<AccountState> _mapGetAccountToState(
      GetAccountOnHomePage event) async* {
    yield AccountInitial();
    yield AccountLoading();
    try {
      final account =
          await _accountService.getUserAccount(event.email, event.password);
      if (account != null) {
        await _analyticsService.logAccountOK();
        yield AccountSuccess(account: account);
      } else {
        yield AccountFailure(error: txt('error_load_profile'));
        await _analyticsService.logAccountKO();
      }
    } on AccountException catch (e) {
      yield AccountFailure(error: txt('error_load_profile'));
    } catch (err) {
      await _analyticsService.logAccountKO();
      yield AccountFailure(error: txt('error_load_profile'));
    }
  }
}
