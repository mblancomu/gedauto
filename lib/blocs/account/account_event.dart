import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AccountEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetAccountOnHomePage extends AccountEvent {
  final String email;
  final String password;

  GetAccountOnHomePage({@required this.email, @required this.password});

  @override
  List<Object> get props => [email, password];
}
