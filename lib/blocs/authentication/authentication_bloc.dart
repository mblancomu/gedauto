import 'package:bloc/bloc.dart';
import 'package:gedauto/locator.dart';
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/services/analytics_service.dart';

import '../../services/services.dart';
import 'authentication_event.dart';
import 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationService _authenticationService;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  AuthenticationBloc(AuthenticationService authenticationService)
      : assert(authenticationService != null),
        _authenticationService = authenticationService,
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppLoaded) {
      yield* _mapAppLoadedToState(event);
    }

    if (event is UserLoggedIn) {
      yield* _mapUserLoggedInToState(event);
      await _analyticsService.logLogin();
    }

    if (event is UserLoggedOut) {
      yield* _mapUserLoggedOutToState(event);
      await _analyticsService.logLogOut();
    }

    if (event is UserCredentials) {
      yield* _mapUserCredentialsToState(event);
      await _analyticsService.logAutoLogin();
    }
  }

  Stream<AuthenticationState> _mapAppLoadedToState(AppLoaded event) async* {
    yield AuthenticationLoading();
    try {
      await Future.delayed(Duration(milliseconds: 500)); // a simulated delay
      final currentUser = await _authenticationService.getCurrentUser();
      final email = await AppPrefs().getUserEmail();
      final password = await AppPrefs().getUserPassword();

      if (currentUser != null ||
          (email != null &&
              email.isNotEmpty &&
              password != null &&
              password.isNotEmpty)) {
        yield AuthenticationAuthenticated(user: currentUser);
      } else {
        yield AuthenticationNotAuthenticated();
      }
    } catch (e) {
      yield AuthenticationFailure(
          message: e.message ?? 'An unknown error occurred');
    }
  }

  Stream<AuthenticationState> _mapUserLoggedInToState(
      UserLoggedIn event) async* {
    yield AuthenticationAuthenticated(user: event.user);
  }

  Stream<AuthenticationState> _mapUserCredentialsToState(
      UserCredentials event) async* {
    yield AuthenticationCredentials();
  }

  Stream<AuthenticationState> _mapUserLoggedOutToState(
      UserLoggedOut event) async* {
    await _authenticationService.signOut();
    yield AuthenticationNotAuthenticated();
  }
}
