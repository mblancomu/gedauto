import 'package:bloc/bloc.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/services/analytics_service.dart';

import '../../exceptions/exceptions.dart';
import '../../locator.dart';
import '../../services/services.dart';
import '../authentication/authentication.dart';
import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthenticationBloc _authenticationBloc;
  final AuthenticationService _authenticationService;
  final AnalyticsService _analyticsService = locator<AnalyticsService>();

  LoginBloc(AuthenticationBloc authenticationBloc,
      AuthenticationService authenticationService)
      : assert(authenticationBloc != null),
        assert(authenticationService != null),
        _authenticationBloc = authenticationBloc,
        _authenticationService = authenticationService,
        super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginInWithEmailButtonPressed) {
      yield* _mapLoginWithEmailToState(event);
    }
  }

  Stream<LoginState> _mapLoginWithEmailToState(
      LoginInWithEmailButtonPressed event) async* {
    yield LoginLoading();
    try {
      final user = await _authenticationService.signInWithEmailAndPassword(
          event.email, event.password);
      if (user != null && user.result == 'OK') {
        AppPrefs().setUserCredentials(event.email, event.password);
        _authenticationBloc.add(UserLoggedIn(user: user));
        await _analyticsService.logLogin();
        yield LoginSuccess(user: user);
      } else {
        yield LoginFailure(error: txt('error_credentials'));
      }
    } on AuthenticationException catch (e) {
      yield LoginFailure(error: txt('error_login'));
    } catch (err) {
      yield LoginFailure(error: txt('error_login'));
    }
  }
}
