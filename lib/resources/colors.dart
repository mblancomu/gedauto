import 'package:flutter/material.dart';

class colors {
  static const primary = Color(0xff3a668a);
  static const primary_light = Color(0xff6a94ba);
  static const primary_dark = Color(0xff003c5d);
  static const accent = Color(0xffe23a32);
  static const white = Color(0xffffffff);
  static const blue_grey = Color(0xffeceff1);
  static const soft_grey = Color(0xfff5f5f5);
  static const dark_grey = Color(0xff757575);
  static const light_grey = Color(0xffc1c1c1);
  static const light_black = Color(0x10000000);
  static const soft_indigo = Color(0xffe8eaf6);
  static const icon_non_active = Colors.grey;
}
