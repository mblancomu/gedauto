import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gedauto/pages/login_page.dart';
import 'package:gedauto/pages/silent_login.dart';
import 'package:gedauto/pages/splash_screen.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/analytics_service.dart';
import 'package:gedauto/widgets/restart_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'blocs/blocs.dart';
import 'language/multilanguage.dart';
import 'locator.dart';
import 'services/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await Firebase.initializeApp();

  if (kDebugMode) {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
  } else {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  }

  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  setupLocator();
  runApp(
      // Injects the Authentication service
      RepositoryProvider<AuthenticationService>(
    create: (context) {
      return FakeAuthenticationService();
    },
    // Injects the Authentication BLoC
    child: BlocProvider<AuthenticationBloc>(
      create: (context) {
        final authService =
            RepositoryProvider.of<AuthenticationService>(context);
        return AuthenticationBloc(authService)..add(AppLoaded());
      },
      child: RestartWidget(
        child: MyApp(),
      ),
    ),
  ));
}

class MyApp extends StatefulWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String email;
  String password;
  SharedPreferences sharedPreferences;
  bool isVerified = false;

  @override
  void initState() {
    super.initState();
    _checkCredentialsInit().then((_) {});
  }

  _verifyLogin() async {
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);
    await {
      if (existCredentials(email, password)) {authBloc.add(UserCredentials())}
    };
    // ignore: close_sinks
  }

  _checkCredentialsInit() async {
    SharedPreferences savedPref = await SharedPreferences.getInstance();
    setState(() {
      email = (savedPref.getString('email') ?? "");
      password = (savedPref.getString('password') ?? "");
    });
  }

  @override
  Widget build(BuildContext context) {
    _verifyLogin();
    return MaterialApp(
      color: colors.primary,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      navigatorObservers: [locator<AnalyticsService>().getAnalyticsObserver()],
      title: 'Grupo Gedauto',
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            // show home page
            return Splash(
              user: state.user,
            );
          }

          if (state is AuthenticationCredentials) {
            // show home page
            return SilentLogin();
          }

          if (state is AuthenticationNotAuthenticated) {
            return LoginAnimatedPage();
          }

          return Container(
              color: Colors.white,
              child: Center(
                  child: CircularProgressIndicator(
                backgroundColor: Colors.red,
              )));
          // otherwise show login page
        },
      ),
      supportedLocales: [Locale('en', ''), Locale('es', '')],
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale?.languageCode == locale?.languageCode &&
              supportedLocale?.countryCode == locale?.countryCode) {
            return supportedLocale;
          }
        }
        var language = Languages.es;
        if (locale.languageCode == 'en') {
          language = Languages.en;
        }
        MultiLanguage.setLanguage(path: language, context: context);
        return supportedLocales?.first;
      },
    );
    //}
  }

  bool existCredentials(String user, String pswd) {
    if (user != null && user.isNotEmpty && pswd != null && pswd.isNotEmpty) {
      return true;
    }
    return false;
  }
}
