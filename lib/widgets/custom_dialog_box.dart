import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/utils/constants.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class CustomDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const CustomDialogBox(
      {Key key, this.title, this.descriptions, this.text, this.img})
      : super(key: key);

  @override
  _CustomDialogBoxState createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius:
            BorderRadius.circular(displayWidth(context) / Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: displayWidth(context) / Constants.padding,
              top: displayHeight(context) / Constants.avatarRadius +
                  displayHeight(context) / Constants.padding,
              right: displayWidth(context) / Constants.padding,
              bottom: displayHeight(context) / Constants.padding),
          margin: EdgeInsets.only(
              top: displayHeight(context) / Constants.avatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(
                  displayWidth(context) / Constants.padding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 0), blurRadius: 0),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: displayWidth(context) * 0.06,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: displayHeight(context) / 40,
              ),
              Text(
                widget.descriptions,
                style: TextStyle(fontSize: displayWidth(context) * 0.04),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: displayHeight(context) / 40,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      widget.text,
                      style: TextStyle(fontSize: displayWidth(context) * 0.05),
                    )),
              ),
            ],
          ),
        ),
        Positioned(
          left: displayWidth(context) / Constants.padding,
          right: displayWidth(context) / Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: displayWidth(context) / Constants.avatarRadius,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(
                    displayWidth(context) / Constants.avatarRadius)),
                child: Image.asset("assets/launcher/icon.png")),
          ),
        ),
      ],
    );
  }
}
