import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/blocs/blocs.dart';
import 'package:gedauto/blocs/login/login_bloc.dart';
import 'package:gedauto/blocs/login/login_event.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/analytics_service.dart';
import 'package:gedauto/utils/network_utils.dart';

class StaggerAnimation extends StatefulWidget {
  final AnalyticsService analyticsService;
  final GlobalKey<FormState> globalKey;
  final String email;
  final String password;
  final LoginState state;

  StaggerAnimation(
      {Key key,
      this.buttonController,
      this.analyticsService,
      this.globalKey,
      this.email,
      this.password,
      this.state})
      : buttonSqueezeanimation = new Tween(
          begin: 320.0,
          end: 70.0,
        ).animate(
          new CurvedAnimation(
            parent: buttonController,
            curve: new Interval(
              0.0,
              0.150,
            ),
          ),
        ),
        buttomZoomOut = new Tween(
          begin: 70.0,
          end: 1000.0,
        ).animate(
          new CurvedAnimation(
            parent: buttonController,
            curve: new Interval(
              0.550,
              0.999,
              curve: Curves.bounceOut,
            ),
          ),
        ),
        containerCircleAnimation = new EdgeInsetsTween(
          begin: const EdgeInsets.only(bottom: 50.0),
          end: const EdgeInsets.only(bottom: 50.0),
        ).animate(
          new CurvedAnimation(
            parent: buttonController,
            curve: new Interval(
              0.500,
              0.800,
              curve: Curves.ease,
            ),
          ),
        ),
        super(key: key);

  final AnimationController buttonController;
  final Animation<EdgeInsets> containerCircleAnimation;
  final Animation buttonSqueezeanimation;
  final Animation buttomZoomOut;

  @override
  _StaggerAnimationState createState() => _StaggerAnimationState(
      analyticsService, globalKey, email, password, state);
}

class _StaggerAnimationState extends State<StaggerAnimation> {
  final AnalyticsService analyticsService;
  final GlobalKey<FormState> globalKey;
  final String email;
  final String password;
  final LoginState state;

  String _email;
  String _password;

  _StaggerAnimationState(this.analyticsService, this.globalKey, this.email,
      this.password, this.state);

  Future<Null> _playAnimation() async {
    try {
      await widget.buttonController.forward();
      await widget.buttonController.reverse();
    } on TickerCanceled {}
  }

  Widget _buildAnimation(BuildContext context, Widget child) {
    return new Padding(
      padding: widget.buttomZoomOut.value == 70
          ? const EdgeInsets.only(bottom: 0.0)
          : widget.containerCircleAnimation.value,
      child: new InkWell(
          onTap: () {
            if (globalKey.currentState.validate()) {
              _playAnimation();
            }
          },
          child: new Hero(
            tag: "fade",
            child: widget.buttomZoomOut.value <= 300
                ? new Container(
                    width: widget.buttomZoomOut.value == 70
                        ? widget.buttonSqueezeanimation.value
                        : widget.buttomZoomOut.value,
                    height: widget.buttomZoomOut.value == 70
                        ? 60.0
                        : widget.buttomZoomOut.value,
                    alignment: FractionalOffset.center,
                    decoration: new BoxDecoration(
                      color: colors.primary,
                      borderRadius: widget.buttomZoomOut.value < 400
                          ? new BorderRadius.all(const Radius.circular(30.0))
                          : new BorderRadius.all(const Radius.circular(0.0)),
                    ),
                    child: widget.buttonSqueezeanimation.value > 75.0
                        ? new Text(
                            txt('page_login'),
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.3,
                            ),
                          )
                        : widget.buttomZoomOut.value < 300.0
                            ? new CircularProgressIndicator(
                                value: null,
                                strokeWidth: 1.0,
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Colors.white),
                              )
                            : null)
                : new Container(
                    width: widget.buttomZoomOut.value,
                    height: widget.buttomZoomOut.value,
                    decoration: new BoxDecoration(
                      shape: widget.buttomZoomOut.value < 500
                          ? BoxShape.circle
                          : BoxShape.rectangle,
                      color: colors.primary,
                    ),
                  ),
          )),
    );
  }

  @override
  void initState() {
    _email = email;
    _password = password;

    super.initState();
    CheckNetwork().checkConnection(context);
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final _loginBloc = BlocProvider.of<LoginBloc>(context);

    Future<Null> _onLoginButtonPressed() async {
      if (globalKey.currentState.validate()) {
        _loginBloc.add(
            LoginInWithEmailButtonPressed(email: _email, password: _password));
      }
    }

    widget.buttonController.addListener(() {
      if (widget.buttonController.isCompleted) {
        state is LoginLoading ? () {} : _onLoginButtonPressed();
      }
    });
    return new AnimatedBuilder(
      builder: _buildAnimation,
      animation: widget.buttonController,
    );
  }
}
