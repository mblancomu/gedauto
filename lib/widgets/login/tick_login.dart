import 'package:flutter/material.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class Tick extends StatelessWidget {
  final DecorationImage image;
  Tick({this.image});
  @override
  Widget build(BuildContext context) {
    return (new Container(
      width: displayWidth(context) / 1.4,
      height: displayHeight(context) / 2.6,
      alignment: Alignment.center,
      decoration: new BoxDecoration(
        image: image,
      ),
    ));
  }
}
