import 'package:flutter/material.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class FormContainer extends StatefulWidget {
  final GlobalKey<FormState> _key;
  final TextEditingController _passwordController;
  final TextEditingController _emailController;
  final _autoValidate;

  const FormContainer(this._key, this._emailController,
      this._passwordController, this._autoValidate);

  @override
  _FormContainerState createState() => _FormContainerState();
}

class _FormContainerState extends State<FormContainer> {
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return (new Container(
      margin: new EdgeInsets.symmetric(horizontal: displayWidth(context) / 15),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new Form(
            key: widget._key,
            autovalidateMode: widget._autoValidate
                ? AutovalidateMode.always
                : AutovalidateMode.disabled,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: txt('email'),
                      labelStyle: TextStyle(
                          color: colors.primary,
                          fontSize: displayWidth(context) * 0.05),
                      icon: new Icon(
                        Icons.email,
                        color: colors.primary,
                      ),
                      contentPadding: EdgeInsets.only(
                          top: displayWidth(context) / 50,
                          right: displayWidth(context) / 50,
                          bottom: displayWidth(context) / 70,
                          left: displayWidth(context) / 50),
                    ),
                    controller: widget._emailController,
                    keyboardType: TextInputType.emailAddress,
                    autocorrect: false,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    style: TextStyle(
                        color: colors.primary,
                        fontSize: displayWidth(context) * 0.05),
                    validator: (value) {
                      if (value.isEmpty) {
                        return txt('empty_email');
                      }
                      if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                          .hasMatch(value)) {
                        return txt('invalid_email');
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        labelText: txt('password'),
                        labelStyle: TextStyle(
                            color: colors.primary,
                            fontSize: displayWidth(context) * 0.05),
                        icon: new Icon(
                          Icons.lock_outline,
                          color: colors.primary,
                        ),
                        contentPadding: EdgeInsets.only(
                            top: displayWidth(context) / 50,
                            right: displayWidth(context) / 50,
                            bottom: displayWidth(context) / 70,
                            left: displayWidth(context) / 50),
                        suffixIcon: IconButton(
                            icon: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: colors.primary,
                            ),
                            onPressed: () {
                              _toggle();
                            })),
                    obscureText: _obscureText,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    controller: widget._passwordController,
                    enableSuggestions: false,
                    autocorrect: false,
                    style: TextStyle(
                        color: colors.primary,
                        fontSize: displayWidth(context) * 0.05),
                    validator: (value) {
                      if (value.isEmpty) {
                        return txt('empty_password');
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: displayHeight(context) / 6,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
