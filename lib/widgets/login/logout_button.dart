import 'package:flutter/material.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class AddButton extends StatelessWidget {
  final Animation<double> buttonGrowAnimation;
  AddButton({this.buttonGrowAnimation});
  @override
  Widget build(BuildContext context) {
    return (new Container(
      width: buttonGrowAnimation.value * displayWidth(context) / 6,
      height: buttonGrowAnimation.value * displayWidth(context) / 6,
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
          color: colors.primary,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.0),
              spreadRadius: 0,
              blurRadius: 0,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ]),
      child: new Icon(
        Icons.logout,
        size: buttonGrowAnimation.value * displayWidth(context) / 10,
        color: colors.white,
      ),
    ));
  }
}
