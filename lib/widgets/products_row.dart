import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/models/product_item.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/date_utils.dart';
import 'package:gedauto/utils/rows_utils.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class ProductsRow extends StatefulWidget {
  final ProductItem product;

  ProductsRow({Key key, this.product}) : super(key: key);

  @override
  _ProductsRowState createState() => _ProductsRowState();
}

class _ProductsRowState extends State<ProductsRow> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: GestureDetector(
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            color: Colors.white,
            elevation: 2,
            child: Column(
              children: [
                Stack(children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(8.0), bottom: Radius.zero),
                    child: Hero(
                      tag: 'image' +
                          RowUtils().createTagForHero(
                              widget.product.titulo ?? "",
                              widget.product.codigo ?? ""),
                      child: Container(
                        height: 180.0,
                        child: ClipRRect(
                          borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0), bottom: Radius.zero),
                          child: ClipRRect(
                            child: Center(
                              child: FadeInImage.assetNetwork(
                                image: widget.product.imagen ?? "",
                                placeholder: "assets/smaller_logo.png",
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 180.0,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        gradient: LinearGradient(
                            begin: FractionalOffset.topCenter,
                            end: FractionalOffset.bottomCenter,
                            colors: [
                              Colors.white.withOpacity(0.0),
                              Colors.white.withOpacity(0.1),
                              Colors.white.withOpacity(0.1),
                              Colors.white,
                            ],
                            stops: [
                              0.1,
                              0.5,
                              0.9,
                              1.0
                            ])),
                  )
                ]),
                new Container(
                  padding: new EdgeInsets.only(
                      top: 0, left: 10, right: 10, bottom: 5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 2, left: 0, right: 4, bottom: 2),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Hero(
                                    tag: 'title' +
                                        RowUtils().createTagForHero(
                                            widget.product.titulo ?? "",
                                            widget.product.codigo ?? ""),
                                    child: Text(widget.product.titulo ?? "",
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize:
                                                displayWidth(context) * 0.05)),
                                  ),
                                )),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 2, left: 4, right: 0, bottom: 2),
                            child: Align(
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  Icons.shopping_cart_rounded,
                                  color: Colors.red,
                                )),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 0, left: 0, right: 4, bottom: 0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Column(
                                  children: [
                                    Hero(
                                      tag: 'subtitle' +
                                          RowUtils().createTagForHero(
                                              widget.product.titulo ?? "",
                                              widget.product.codigo ?? ""),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "Cod: " + widget.product.codigo ?? "",
                                          textAlign: TextAlign.left,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontWeight: FontWeight.normal,
                                              color: Colors.black54,
                                              fontSize:
                                                  displayWidth(context) * 0.04),
                                        ),
                                      ),
                                    ),
                                    Hero(
                                      tag: 'date' +
                                          RowUtils().createTagForHero(
                                              widget.product.fechaPublicacion ??
                                                  "",
                                              widget.product.codigo ?? ""),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 5,
                                              left: 0,
                                              right: 4,
                                              bottom: 0),
                                          child: Text(
                                            DateConvertUtils().convertDateTime(
                                                widget
                                                    .product.fechaPublicacion),
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                color: Colors.grey,
                                                fontSize:
                                                    displayWidth(context) *
                                                        0.035),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: 0, left: 4, right: 0, bottom: 0),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: ElevatedButton(
                                  child: Text('+ Info',
                                      style: TextStyle(fontSize: 14)),
                                  style: ButtonStyle(
                                      foregroundColor:
                                          MaterialStateProperty.all<Color>(
                                              Colors.white),
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              colors.primary),
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                      ))),
                                  onPressed: () => RowUtils().launchDetail(
                                      context, widget.product, Colors.red)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            )),
      ),
    );
  }
}
