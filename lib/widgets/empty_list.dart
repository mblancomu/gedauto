import 'package:flutter/material.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class EmptyList extends StatelessWidget {
  final String message;

  const EmptyList(this.message);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Center(
        child: Text(message != null ? message : "",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: displayWidth(context) * 0.05)),
      ),
    );
  }
}
