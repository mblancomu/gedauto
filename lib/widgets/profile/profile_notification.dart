import 'package:flutter/material.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class ProfileNotification extends StatelessWidget {
  final Animation<double> containerGrowAnimation;
  final DecorationImage profileImage;
  ProfileNotification({this.containerGrowAnimation, this.profileImage});
  @override
  Widget build(BuildContext context) {
    return (new Container(
        width: containerGrowAnimation.value * displayWidth(context) / 2.2,
        height: containerGrowAnimation.value * displayHeight(context) / 4,
        decoration: new BoxDecoration(
          color: colors.white,
          shape: BoxShape.circle,
          image: profileImage,
        )));
  }
}
