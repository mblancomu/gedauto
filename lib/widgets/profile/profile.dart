import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/language/multilanguage.dart';
import 'package:gedauto/models/account.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/resources/delete_icon_icons.dart';

import '../confirm_remove_account_dialog.dart';

class Profile extends StatefulWidget {
  final Account account;

  Profile({Key key, this.account});

  @override
  State<StatefulWidget> createState() => _ProfilePageState(account);
}

class _ProfilePageState extends State<Profile> {
  final Account account;

  _ProfilePageState(this.account);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    return Container(
      width: _width,
      height: _height / 1.7,
      child: new Center(
        child: new Column(
          children: <Widget>[
            new SizedBox(
              height: _height / 35,
            ),
            new Text(
              account.nombre ?? "",
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: _width / 15,
                  color: colors.primary_dark),
            ),
            new Padding(
              padding: new EdgeInsets.only(
                  top: 10,
                  left: _width / 8,
                  right: _width / 8,
                  bottom: _width / 100),
              child: new Text(
                account.email ?? "",
                style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: _width / 25,
                    color: colors.primary_dark),
                textAlign: TextAlign.center,
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(
                  top: 10,
                  left: _width / 8,
                  right: _width / 8,
                  bottom: _width / 40),
              child: new Text(
                "NIF " + account.nif ?? "",
                style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: _width / 25,
                    color: colors.primary_dark),
                textAlign: TextAlign.center,
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(
                  top: 10,
                  left: _width / 8,
                  right: _width / 8,
                  bottom: _width / 40),
              child: new TextButton.icon(
                label: Text(
                  txt('title_remove_popup'),
                  style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: _width / 24,
                      color: Colors.red),
                ),
                icon: Icon(
                  DeleteIcon.delete_forever,
                  color: Colors.red,
                ),
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.red))),
                    padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.all(12)),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(colors.light_black)),
                onPressed: () {
                  showDeleteDialog(context);
                },
              ),
            ),
            new Divider(
              height: _height / 35,
              color: colors.primary_dark,
            ),
            new Row(
              children: <Widget>[
                rowCell(
                    txt('vip_code'),
                    getVipCardCode(account.codigoVip ?? "",
                        account.tarjetaBloqueada ?? 0)),
              ],
            ),
            new Divider(height: _height / 35, color: colors.primary_dark),
            new Opacity(
              opacity: getOpacityForBlockText(account.tarjetaBloqueada ?? 0),
              child: new Padding(
                padding: new EdgeInsets.only(
                    top: 20, left: _width / 8, right: _width / 8),
                child: new Text(
                  txt('vip_card_blocked'),
                  style: new TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: _width / 25,
                      color: Colors.red),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getVipCardCode(String code, int status) {
    if (isCardBlocked(status)) {
      return txt('blocked_card');
    }
    return code;
  }

  double getOpacityForBlockText(int status) {
    if (isCardBlocked(status)) {
      return 1.0;
    }
    return 0.0;
  }

  bool isCardBlocked(int status) {
    if (status == 1) {
      return true;
    }
    return false;
  }

  Widget rowCell(String section, String type) => new Expanded(
          child: new Column(
        children: <Widget>[
          new Text(
            '$section',
            style: new TextStyle(
                color: Colors.redAccent,
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: new EdgeInsets.only(top: 10),
          ),
          TypewriterAnimatedTextKit(
              speed: Duration(milliseconds: 600),
              isRepeatingAnimation: false,
              text: [type],
              textStyle: TextStyle(
                  color: colors.primary_dark,
                  fontSize: 30,
                  fontWeight: FontWeight.normal)),
        ],
      ));
}
