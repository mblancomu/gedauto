import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/blocs/authentication/authentication_bloc.dart';
import 'package:gedauto/blocs/authentication/authentication_event.dart';
import 'package:gedauto/prefs/shared_prefs.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/services/analytics_service.dart';

import '../restart_app.dart';

class StaggerAnimation extends StatefulWidget {
  final AnalyticsService analyticsService;

  StaggerAnimation({Key key, this.buttonController, this.analyticsService})
      : buttonZoomOutAnimation = new Tween(
          begin: 60.0,
          end: 1000.0,
        ).animate(
          new CurvedAnimation(parent: buttonController, curve: Curves.easeOut),
        ),
        buttonBottomtoCenterAnimation = new AlignmentTween(
          begin: Alignment.bottomRight,
          end: Alignment.center,
        ).animate(
          new CurvedAnimation(
            parent: buttonController,
            curve: new Interval(
              0.0,
              0.200,
              curve: Curves.easeOut,
            ),
          ),
        ),
        super(key: key);

  final Animation<double> buttonController;
  final Animation buttonZoomOutAnimation;
  final Animation<Alignment> buttonBottomtoCenterAnimation;

  @override
  _StaggerAnimationState createState() =>
      _StaggerAnimationState(analyticsService);
}

class _StaggerAnimationState extends State<StaggerAnimation> {
  final AnalyticsService analyticsService;

  _StaggerAnimationState(this.analyticsService);

  Widget _buildAnimation(BuildContext context, Widget child) {
    timeDilation = 0.4;

    return (new Padding(
        padding: widget.buttonZoomOutAnimation.value < 400
            ? new EdgeInsets.all(20.0)
            : new EdgeInsets.all(0.0),
        child: new Container(
            alignment: widget.buttonBottomtoCenterAnimation.value,
            child: new InkWell(
              child: new Container(
                width: widget.buttonZoomOutAnimation.value,
                height: widget.buttonZoomOutAnimation.value,
                alignment: widget.buttonBottomtoCenterAnimation.value,
                decoration: new BoxDecoration(
                    color: colors.primary,
                    shape: widget.buttonZoomOutAnimation.value < 400
                        ? BoxShape.circle
                        : BoxShape.rectangle),
                child: new Icon(
                  Icons.add,
                  size: widget.buttonZoomOutAnimation.value < 50
                      ? widget.buttonZoomOutAnimation.value
                      : 0.0,
                  color: Colors.white,
                ),
              ),
            ))));
  }

  _removeCredentials() {
    setState(() {
      AppPrefs().removeUserCredentials();
    });
  }

  @override
  Widget build(BuildContext context) {
    // ignore: close_sinks
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);

    widget.buttonController.addListener(() {
      // if (controller.isCompleted) Navigator.pushNamed(context, "/login");    //options
      // if (controller.isCompleted) Navigator.of(context).pop();       //options
      if (widget.buttonController.isCompleted) {
        analyticsService.logLogOut();
        authBloc.add(UserLoggedOut());
        _removeCredentials();
        RestartWidget.of(context).restartApp();
        // Navigator.pushAndRemoveUntil(
        //     context,
        //     MaterialPageRoute(builder: (context) => MyApp()),
        //     ModalRoute.withName('/myApp'));
      }
    });
    return new AnimatedBuilder(
      builder: _buildAnimation,
      animation: widget.buttonController,
    );
  }
}
