import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gedauto/resources/colors.dart';

import '../blocs/authentication/authentication_bloc.dart';
import '../language/multilanguage.dart';
import '../utils/constants.dart';
import '../utils/sizes_helpers.dart';

showDeleteDialog(context) async {
  await showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(displayWidth(context) / Constants.padding),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: contentBox(context),
      );
    },
  );
}

contentBox(context) {
  final authBloc = BlocProvider.of<AuthenticationBloc>(context);

  return Stack(
    children: <Widget>[
      Container(
        padding: EdgeInsets.only(
            left: displayWidth(context) / Constants.padding,
            top: displayHeight(context) / Constants.avatarRadius +
                displayHeight(context) / Constants.padding,
            right: displayWidth(context) / Constants.padding,
            bottom: displayHeight(context) / 30),
        margin: EdgeInsets.only(
            top: displayHeight(context) / Constants.avatarRadius),
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.red,
            borderRadius: BorderRadius.circular(
                displayWidth(context) / Constants.padding),
            boxShadow: [
              BoxShadow(
                  color: Colors.black, offset: Offset(0, 0), blurRadius: 0),
            ]),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              txt('title_remove_popup'),
              style: TextStyle(
                  fontSize: displayWidth(context) * 0.06,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            SizedBox(
              height: displayHeight(context) / 40,
            ),
            Text(
              txt('warning_remove_account'),
              style: TextStyle(
                  fontSize: displayWidth(context) * 0.04, color: colors.white),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: displayHeight(context) / 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      txt('btn_cancel_dialog'),
                      style: TextStyle(
                          fontSize: displayWidth(context) * 0.05,
                          color: Colors.white),
                    )),
                TextButton(
                    onPressed: () {
                      // TODO: Endpoint para eliminar la cuenta.

/*                      authBloc.add(UserLoggedOut());
                      AppPrefs().removeUserCredentials();
                      RestartWidget.of(context).restartApp();*/
                    },
                    child: Text(
                      txt('btn_ok_dialog'),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: displayWidth(context) * 0.05,
                          color: Colors.white),
                    )),
              ],
            ),
          ],
        ),
      ),
      Positioned(
        left: displayWidth(context) / 15,
        right: displayWidth(context) / 15,
        height: displayHeight(context) / 14,
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: displayWidth(context) / 12,
          child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(displayWidth(context) / 12)),
              child: Image.asset("assets/launcher/icon.png")),
        ),
      ),
    ],
  );
}
