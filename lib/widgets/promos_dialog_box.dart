import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gedauto/resources/colors.dart';
import 'package:gedauto/utils/constants.dart';
import 'package:gedauto/utils/sizes_helpers.dart';

class PromosDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const PromosDialogBox(
      {Key key, this.title, this.descriptions, this.text, this.img})
      : super(key: key);

  @override
  _PromosDialogBoxState createState() => _PromosDialogBoxState();
}

class _PromosDialogBoxState extends State<PromosDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius:
            BorderRadius.circular(displayWidth(context) / Constants.padding),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: displayWidth(context) / Constants.padding,
              top: displayHeight(context) / Constants.avatarRadius +
                  displayHeight(context) / Constants.padding,
              right: displayWidth(context) / Constants.padding,
              bottom: displayHeight(context) / 30),
          margin: EdgeInsets.only(
              top: displayHeight(context) / Constants.avatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: colors.primary_dark,
              borderRadius: BorderRadius.circular(
                  displayWidth(context) / Constants.padding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 0), blurRadius: 0),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: displayWidth(context) * 0.06,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              SizedBox(
                height: displayHeight(context) / 40,
              ),
              Text(
                widget.descriptions,
                style: TextStyle(
                    fontSize: displayWidth(context) * 0.04,
                    color: colors.light_grey),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: displayHeight(context) / 40,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      widget.text,
                      style: TextStyle(
                          fontSize: displayWidth(context) * 0.05,
                          color: Colors.white),
                    )),
              ),
            ],
          ),
        ),
        Positioned(
          left: displayWidth(context) / 15,
          right: displayWidth(context) / 15,
          height: displayHeight(context) / 14,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: displayWidth(context) / 12,
            child: ClipRRect(
                borderRadius: BorderRadius.all(
                    Radius.circular(displayWidth(context) / 12)),
                child: Image.asset("assets/launcher/icon.png")),
          ),
        ),
      ],
    );
  }
}
