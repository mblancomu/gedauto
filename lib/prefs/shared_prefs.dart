import 'package:shared_preferences/shared_preferences.dart';

class AppPrefs {
  Future getSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  setUserCredentials(String email, String password) async {
    SharedPreferences prefs = await getSharedPreferences();
    prefs.setString('email', email).then((value) => email);
    prefs.setString('password', password).then((value) => password);
  }

  removeUserCredentials() async {
    SharedPreferences prefs = await getSharedPreferences();
    await prefs.clear();
  }

  Future<String> getUserEmail() async {
    SharedPreferences prefs = await getSharedPreferences();
    return prefs.getString('email') ?? "";
  }

  Future<String> getUserPassword() async {
    SharedPreferences prefs = await getSharedPreferences();
    return prefs.getString('password') ?? "";
  }

  setDateLastItemPromo(String date) async {
    SharedPreferences prefs = await getSharedPreferences();
    prefs.setString('dateLastItemPromo', date);
  }

  getDateLastItemPromo() async {
    SharedPreferences prefs = await getSharedPreferences();
    String date = prefs.getString('dateLastItemPromo') ?? "";
    return date;
  }

  removeDateLastItemPromo() async {
    SharedPreferences prefs = await getSharedPreferences();
    prefs.remove('dateLastItemPromo');
  }

  setDateLastItemDiscount(String date) async {
    SharedPreferences prefs = await getSharedPreferences();
    prefs.setString('dateLastItemDiscount', date);
  }

  getDateLastItemDiscount() async {
    SharedPreferences prefs = await getSharedPreferences();
    String date = prefs.getString('dateLastItemDiscount') ?? "";
    return date;
  }

  removeDateLastItemDiscount() async {
    SharedPreferences prefs = await getSharedPreferences();
    prefs.remove('dateLastItemDiscount');
  }

  checkIfExistPref(String prefValue) async {
    SharedPreferences prefs = await getSharedPreferences();
    bool exist = prefs.containsKey(prefValue);
    return exist;
  }
}
