import 'package:gedauto/models/product_item.dart';

class Product {
  String result;
  List<ProductItem> listado;

  Product({this.result, this.listado});

  factory Product.fromJson(Map<String, dynamic> parsedJson) {
    var listadoFromJson = parsedJson['listado'] as List;
    List<ProductItem> itemsList = [];

    if (listadoFromJson != null) {
      itemsList = listadoFromJson.map((i) => ProductItem.fromJson(i)).toList();
    }

    return new Product(
      result: parsedJson['result'],
      listado: itemsList,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Product &&
          runtimeType == other.runtimeType &&
          result == other.result;

  @override
  int get hashCode => result.hashCode;
}
