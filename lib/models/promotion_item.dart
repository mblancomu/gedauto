class PromotionItem {
  final String fechaPublicacion;
  final String codigo;
  final String texto;
  final String imagen;

  PromotionItem({this.fechaPublicacion, this.codigo, this.texto, this.imagen});

  factory PromotionItem.fromJson(Map<String, dynamic> parsedJson) {
    return PromotionItem(
        fechaPublicacion: parsedJson['fecha'],
        texto: parsedJson['texto'],
        codigo: parsedJson['codigo'],
        imagen: parsedJson['imagen']);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PromotionItem &&
          runtimeType == other.runtimeType &&
          codigo == other.codigo;

  @override
  int get hashCode => codigo.hashCode;
}
