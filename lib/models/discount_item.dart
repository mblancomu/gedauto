class DiscountItem {
  final String fechaPublicacion;
  final String codigo;
  final String texto;
  final String imagen;

  DiscountItem({this.fechaPublicacion, this.codigo, this.texto, this.imagen});

  factory DiscountItem.fromJson(Map<String, dynamic> parsedJson) {
    return DiscountItem(
        fechaPublicacion: parsedJson['fecha'],
        texto: parsedJson['texto'],
        codigo: parsedJson['codigo'],
        imagen: parsedJson['imagen']);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiscountItem &&
          runtimeType == other.runtimeType &&
          codigo == other.codigo;

  @override
  int get hashCode => codigo.hashCode;
}
