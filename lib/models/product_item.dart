class ProductItem {
  String fechaPublicacion;
  String codigo;
  String titulo;
  String imagen;
  String descripcion;
  String zoom;
  String enlaceUrl;
  String enlaceTexto;
  int w;
  int h;

  ProductItem(
      {this.fechaPublicacion,
      this.codigo,
      this.titulo,
      this.imagen,
      this.descripcion,
      this.zoom,
      this.enlaceUrl,
      this.enlaceTexto,
      this.w,
      this.h});

  factory ProductItem.fromJson(Map<String, dynamic> parsedJson) {
    return ProductItem(
        fechaPublicacion: parsedJson['fecha'],
        titulo: parsedJson['titulo'],
        codigo: parsedJson['codigo'],
        imagen: parsedJson['imagen'],
        descripcion: parsedJson['descripcion'],
        zoom: parsedJson['zoom'],
        enlaceUrl: parsedJson['enlace_url'],
        enlaceTexto: parsedJson['enlace_texto'],
        w: parsedJson['w'],
        h: parsedJson['h']);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductItem &&
          runtimeType == other.runtimeType &&
          codigo == other.codigo;

  @override
  int get hashCode => codigo.hashCode;
}
