class Account {
  String nombre;
  String email;
  String password;
  String result;
  String codigoVip;
  String nif;
  int tarjetaBloqueada;
  String notificacion;

  Account(
      {this.nombre,
      this.email,
      this.password,
      this.result,
      this.codigoVip,
      this.nif,
      this.tarjetaBloqueada,
      this.notificacion});

  factory Account.fromJson(Map<String, dynamic> parsedJson, String psw) {
    return Account(
        nombre: parsedJson['nombre'],
        result: parsedJson['result'],
        codigoVip: parsedJson['codigo_vip'],
        nif: parsedJson['nif'],
        tarjetaBloqueada: parsedJson['tarjeta_bloqueada'],
        notificacion: parsedJson['notificacion'],
        password: psw,
        email: parsedJson['email']);
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Account && runtimeType == other.runtimeType && nif == other.nif;

  @override
  int get hashCode => nombre.hashCode;
}
